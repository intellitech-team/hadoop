Nowadays it is argued that the last two eras have witnessed an unprecedented 
revolution in technological advances The latter have affected all
aspects of people is lives starting from individuals to national
and international policies For instance according to the European Commission
the ICT sector is directly responsible for of European GDP with a market value 
of Billion Euro annually At the same time the social impact of ICT has become
significant For example the fact that there are millions daily internet users in Europe
and virtually all Europeans own a mobile phone have changed lifestyles
Actually the technological revolution in the last quarter of the twentieth century
has been blessed for the virtues of increasing productivity
In brief technology lies at the heart of the globalisation process
affecting education work and culture according to Graddol
