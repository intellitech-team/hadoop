package com.intellitech.training.account.reader;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.LineRecordReader;

import com.intellitech.training.account.model.IdNameWritable;

public class NameRecordReader extends RecordReader<LongWritable,IdNameWritable >{

	LineRecordReader lineRecordReader;
	LongWritable key;
	IdNameWritable value;
	TaskAttemptContext context;
	
	@Override
	public void initialize(InputSplit inputSplit, TaskAttemptContext context) throws IOException, InterruptedException {
		this.context = context;
		lineRecordReader = new LineRecordReader();
		lineRecordReader.initialize(inputSplit, context);
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		
		if (!lineRecordReader.nextKeyValue()) {
			return false;
		}	
		
		String line = lineRecordReader.getCurrentValue().toString(); 
		System.out.println("line " + line);
		String[] allFields = line.split("\t");
		
		key = new LongWritable(Long.parseLong(allFields[0]));
		value = new IdNameWritable(allFields[0],allFields[1]);
		return true;
	}

	@Override
	public LongWritable getCurrentKey() throws IOException, InterruptedException {
		return key;
	}

	@Override
	public IdNameWritable getCurrentValue() throws IOException, InterruptedException {
		return value;
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		return lineRecordReader.getProgress();
	}

	@Override
	public void close() throws IOException {
		lineRecordReader.close();
	}
}
