package com.intellitech.training.account.model;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class IdEmailWritable implements WritableComparable<IdEmailWritable>{

	private LongWritable id;
	private Text email;
	 
	public IdEmailWritable() {
		this.id = new LongWritable();
		this.email = new Text();
	}
	
	public IdEmailWritable(String id, String valeur) {
		this.id = new LongWritable(Long.parseLong(id));
		this.email = new Text(valeur);
	}
	
	public LongWritable getId() {
		return id;
	}

	public void setId(LongWritable id) {
		this.id = id;
	}


	@Override
	public int hashCode() {
		return id.hashCode() + email.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof IdEmailWritable) {
			IdEmailWritable doubleIntWritable = (IdEmailWritable) obj;
			return this.getId().equals(doubleIntWritable.getId())
					&& this.email.equals(doubleIntWritable.email);
		}
		return false;
	}

	@Override
	public String toString() {
		return " postal code :  "+ id.toString()+ " price :  "+ email.toString();
	}
	@Override
	public void readFields(DataInput dataInput) throws IOException {
		id.readFields(dataInput);
		email.readFields(dataInput);
	}

	@Override
	public void write(DataOutput dataOutput) throws IOException {
		id.write(dataOutput);
		email.write(dataOutput);
	}

	@Override
	public int compareTo(IdEmailWritable iDEmailWritable) {
	int codeCmp=id.compareTo(iDEmailWritable.getId());
	if(codeCmp!=0){
		return codeCmp;
	}else {
		return email.compareTo(iDEmailWritable.getEmail());
	}
		
	}

	public Text getEmail() {
		return email;
	}

	public void setEmail(Text email) {
		this.email = email;
	}
}
