package com.intellitech.training.account;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.intellitech.training.account.model.IdNameWritable;

public class NameMapper
		extends Mapper<LongWritable,IdNameWritable,LongWritable,Text> {

	@Override
	protected void map(LongWritable key, IdNameWritable value,
			Mapper<LongWritable, IdNameWritable, LongWritable, Text>.Context context)
			throws IOException, InterruptedException {
		System.out.println("name map " + value.getName());
		context.write(value.getId(), value.getName());
	}



}
