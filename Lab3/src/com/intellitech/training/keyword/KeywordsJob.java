package com.intellitech.training.keyword;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class KeywordsJob extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		
		if (args.length != 2) {
			System.err.println("error");
			return -1;
		}

		Configuration conf = getConf();
		conf.set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", "\t");
		Job job = Job.getInstance(conf, "lab3");
		job.setJarByClass(KeywordsJob.class);

		job.setInputFormatClass(KeyValueTextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		TextInputFormat.setInputPaths(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setMapperClass(KeywordsMapper.class);
		job.setReducerClass(KeywordsReducer.class);
		job.setCombinerClass(KeywordsReducer.class);
		
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		String inputPath = "hdfs://localhost:9000/training/lab3/inputs/*";
		String outputPath = "hdfs://localhost:9000/training/lab3/output" ;
		int exitCode = ToolRunner.run(new KeywordsJob(), new String[] { inputPath,
				outputPath});

		System.exit(exitCode);

	}
}
