package com.intellitech.training.keyword.multipleoutputs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.intellitech.training.keyword.KeywordsMapper;

public class KeywordsJob extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {

		Configuration conf = new Configuration();
		conf.set("mapreduce.input.keyvaluelinerecordreader.key.value.separator", "\t");
		Job job = Job.getInstance(conf, "lab3");
		job.setJarByClass(KeywordsJob.class);

		job.setInputFormatClass(KeyValueTextInputFormat.class);
		job.setOutputFormatClass(LazyOutputFormat.class);

		MultipleOutputs.addNamedOutput(job, "text", TextOutputFormat.class, Text.class, Text.class);
		MultipleOutputs.addNamedOutput(job, "seq", SequenceFileOutputFormat.class, Text.class, Text.class);
		TextInputFormat.setInputPaths(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setMapperClass(KeywordsMapper.class);
		job.setReducerClass(KeywordsReducer.class);
		job.setCombinerClass(com.intellitech.training.keyword.KeywordsReducer.class);

		int r = job.waitForCompletion(true) ? 0 : 1;

		System.out.println(job.getCounters().findCounter("UserCounters", "badInputs").getValue());
		return r;
	}

	public static void main(String[] args) throws Exception {
		String inputPath = "hdfs://localhost:9000/training/lab3/inputs/*";
		String outputPath = "hdfs://localhost:9000/training/lab3/output";
		int exitCode = ToolRunner.run(new KeywordsJob(), new String[] { inputPath, outputPath });
		System.exit(exitCode);
	}
}
