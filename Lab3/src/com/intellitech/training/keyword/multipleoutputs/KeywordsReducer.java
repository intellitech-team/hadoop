package com.intellitech.training.keyword.multipleoutputs;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

public class KeywordsReducer extends Reducer<Text, Text, Text, Text> {

	private MultipleOutputs<Text, Text> multipleOutputs;

	@Override
	protected void setup(Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {
		multipleOutputs = new MultipleOutputs<Text, Text>(context);
	}

	@Override
	protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {

		StringBuilder sb = new StringBuilder();
		for (Text value : values) {
			sb.append(value);
		}
		sb.setLength(sb.length() - 1);

		multipleOutputs.write("text", key, new Text(sb.toString()));
		multipleOutputs.write("seq", key, new Text(sb.toString()));
	}

	@Override
	protected void cleanup(Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {

		multipleOutputs.close();
	}
}
