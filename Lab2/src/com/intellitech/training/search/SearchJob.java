package com.intellitech.training.search;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

// role of the job: search for the number of occurence of a word in input files
// and write the result in hdfs
public class SearchJob extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		if (args.length != 3) {
			
			System.err.println("error");
			return -1;
		}
		Configuration conf = getConf();
		conf.set("searchingWord", args[2]);
		
		Job job = Job.getInstance(conf, "lab2");
		job.setJarByClass(SearchJob.class);

		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		TextOutputFormat.setOutputPath(job, new Path(args[1]));
		TextInputFormat.setInputPaths(job, new Path(args[0]));
				
		job.setMapperClass(SearchMapper.class);
		job.setReducerClass(SearchReducer.class);
		job.setCombinerClass(SearchCombiner.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);

		Path outputFile = new Path(args[1]);
		outputFile.getFileSystem(conf).delete(outputFile, true);
		
		int r = job.waitForCompletion(true) ? 0 : 1;
		
		System.out.println(job.getCounters().findCounter("UserCounters","numberOfLines").getValue());
		return r;
	}

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new SearchJob(), new String[] { "hdfs://localhost:9000/training/lab2/inputs",
				"hdfs://localhost:9000/training/lab2/output","the" });
		System.exit(exitCode);

	}

}


