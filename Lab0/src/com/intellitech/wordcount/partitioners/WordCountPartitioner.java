package com.intellitech.wordcount.partitioners;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

public class WordCountPartitioner extends Partitioner<Text, IntWritable>{

	@Override
	public int getPartition(Text key, IntWritable value, int numPartitions) {
		
		char decider = key.toString().toUpperCase().charAt(0);
		char A = 'A';
		char L = 'L';
		if((A<=decider) && (decider<=L)) {
			return 0;
		} else {
			return 1;
		}
	}
}
