package com.intellitech.averagetemperature.jobs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.intellitech.averagetemperature.mappers.AverageTemperatureMapper;
import com.intellitech.averagetemperature.reducers.AverageTemperatureReducer;

public class AverageTemperatureJob extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		
		Configuration conf = new Configuration();

		Job job = Job.getInstance(, "AverageTemperatureJob");
		job.setJarByClass(.class);

		job.setInputFormatClass(.class);
		job.setOutputFormatClass(.class);

		TextOutputFormat.setOutputPath(job, new Path(args[1]));
		TextInputFormat.setInputPaths(job, new Path(args[0]));

		TextOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setMapperClass(.class);
		job.setReducerClass(.class);
		
		job.setOutputKeyClass(.class);
		job.setOutputValueClass(.class);

		Path outputFile = new Path(args[1]);
		outputFile.getFileSystem(conf).delete(outputFile, true);
	 
		return job.waitForCompletion(true) ? 0 : 1;
	}

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new AverageTemperatureJob(), new String [] {
				"hdfs://localhost:9000/training/AverageTemperatre/*",
				"hdfs://localhost:9000/training/AverageTemperatre/output"
		});
		System.exit(exitCode);

	}
}
