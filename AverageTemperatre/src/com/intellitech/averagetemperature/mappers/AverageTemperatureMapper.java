package com.intellitech.averagetemperature.mappers;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.commons.lang.StringUtils;

public class AverageTemperatureMapper extends Mapper<, , , > {

	@Override
	protected void map( key,  value, Context context)
			throws IOException, InterruptedException {

		String[] line = value.toString().split(",");

		String datePart = line[1];
		String temp = line[10];

		if (StringUtils.isNumeric(temp)) {
			context.write(
					new Text(),
					new IntWritable()
			);
		}
	}
}