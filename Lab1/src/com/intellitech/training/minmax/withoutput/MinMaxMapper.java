package com.intellitech.training.minmax.withoutput;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

// the role of the mapper, it's to get the data from the input 
// and to filter it and organize it in (key, value) pairs
public class MinMaxMapper extends Mapper<LongWritable,Text,IntWritable,IntWritable>{
	
	@Override
	protected void map(LongWritable key, Text value,
			Mapper<LongWritable, Text, IntWritable, IntWritable>.Context context)
					throws IOException, InterruptedException {		
		
		String sValue = value.toString();
		try{
			int num = Integer.parseInt(sValue);
			context.write(new IntWritable(1), new IntWritable(num));
		}catch(Exception e ){
			context.getCounter("UserCounters","badInputs").increment(1);
		}
		
	}
}