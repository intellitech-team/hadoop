package com.intellitech.training.minmax.withoutput;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class MinMaxJob extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {

		// we would like that we get exactly too args (inputPath and outputPath)
		// so if the number of args !=2 we will print an error
		if (args.length != 2) {
			System.err.println("error");
			return -1;
		}
		// getConf method is inherited from Configured class, that's why we should
		// extends Configured
		Configuration conf = getConf();
		Job job = Job.getInstance(conf, "lab1");
		// Give the job the name of the main class
		job.setJarByClass(MinMaxJob.class);

		// Specify the input format, which will have impact on the key and value
		// types of the mapper inputs. because by specifying TextInputFormat as an input
		// the mapper key will be LongWritable which equal to the offset in the file input
		// and the mapper value will be TextWritable which equal to the line itself
		job.setInputFormatClass(TextInputFormat.class);

		// By specifying TextOutputFormat as an output the format of your file will be
		// Key.toString()then 4 spaces then value.toString()
		job.setOutputFormatClass(TextOutputFormat.class);

		// specify the input paths in the hdfs
		TextInputFormat.setInputPaths(job, new Path(args[0]));
		// specify the output paths in the hdfs
		// We need to ensure that the output file doesn't already exist
		// because if we run a job and give it an output that is already exist,
		// the job will fail
		// and it's a great policy because if it will run the job, the already
		// existing file will be deleted :o
		
		TextOutputFormat.setOutputPath(job, new Path(args[1]));

		// Give the job the name of the mapper class
		job.setMapperClass(MinMaxMapper.class);
		// Give the job the name of the reducer class
		job.setReducerClass(MinMaxReducer.class);

		// set the key output type of the mapper
		job.setMapOutputKeyClass(IntWritable.class);
		// set the value output type of the mapper
		job.setMapOutputValueClass(IntWritable.class);

		// set the key output type of the reducer
		job.setOutputKeyClass(NullWritable.class);
		// set the value output type of the reducer
		job.setOutputValueClass(Text.class);

		// run the job
		int r = job.waitForCompletion(true) ? 0 : 1;

		// print the number of bad records on the screen
		System.out.println(job.getCounters().findCounter("UserCounters", "badInputs").getValue());
		return r;
	}

	public static void main(String[] args) throws Exception {
		String inputPaths = "hdfs://localhost:9000/training/lab1/inputs/*";
		String outputPath = "hdfs://localhost:9000/training/lab1/output";
		ToolRunner.run(new MinMaxJob(), new String[] { inputPaths, outputPath });
	}
}