package com.intellitech.training.minmax.withoutput;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MinMaxReducer 

extends Reducer<IntWritable,IntWritable,NullWritable,Text>{
	
	@Override
	protected void reduce(IntWritable key, Iterable<IntWritable> values,
			Reducer<IntWritable, IntWritable, NullWritable, Text>.Context context)
					throws IOException, InterruptedException {
		int max = values.iterator().next().get();
		int min = max;
		for(IntWritable value : values){
			if(value.get() > max){
				max = value.get();
			}
			if(value.get() <  min){
				min = value.get();
			}
		}
		context.write( NullWritable.get(), new Text(min + " " + max));
	}

}
