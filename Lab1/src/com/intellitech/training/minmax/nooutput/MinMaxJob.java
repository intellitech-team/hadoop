package com.intellitech.training.minmax.nooutput;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.NullOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.intellitech.training.minmax.withoutput.MinMaxMapper;

public class MinMaxJob extends Configured implements Tool {
	
	@Override
	public int run(String[] args) throws Exception {

		if (args.length != 1) {
			System.err.println("error");
			return -1;
		}
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "lab1");
		// Give the job the name of the main class 
		job.setJarByClass(MinMaxJob.class);

		// Specify the input format, which will have impact on the key and value types
		// of the mapper inputs.
		job.setInputFormatClass(TextInputFormat.class);
		// Specify the output format, which will have impact on the key and value types
		// of the reducer outputs.
		job.setOutputFormatClass(NullOutputFormat.class);

		// specify the input paths in the hdfs
		TextInputFormat.setInputPaths(job, new Path(args[0]));
		
		// Give the job the name of the mapper class
		job.setMapperClass(MinMaxMapper.class);
		// Give the job the name of the reducer class
		job.setReducerClass(MinMaxReducer.class);
		// Give the job the name of the combiner class
		job.setCombinerClass(Combiner.class);

		// set the key output type of the mapper
		job.setMapOutputKeyClass(IntWritable.class);
		// set the value output type of the mapper
		job.setMapOutputValueClass(IntWritable.class);
		// set the key output type of the reducer
		job.setOutputKeyClass(NullWritable.class);
		// set the value output type of the reducer
		job.setOutputValueClass(NullWritable.class);

		
		// run the job
		int r = job.waitForCompletion(true) ? 0 : 1;
		// print the values of counters
		System.out.println(job.getCounters().findCounter("UserCounters","badInputs").getValue());
		System.out.println(job.getCounters().findCounter("result", "min").getValue());
		System.out.println(job.getCounters().findCounter("result", "max").getValue());
		
		return r;
	}
	
	public static void main(String[] args) throws Exception {
		String inputPaths = "hdfs://localhost:9000/training/TP1inputs/*";
		ToolRunner.run(new MinMaxJob(), new String[] {inputPaths});
	}

}
