package com.intellitech.training.exam.input;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.LineRecordReader;
import com.intellitech.training.exam.FlightDate;

public class FlightReader extends RecordReader<FlightDate, Text> {
	
	FlightDate key;
	Text value;

	LineRecordReader lineRecordReader;
	@Override
	public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
		lineRecordReader = new LineRecordReader();
		lineRecordReader.initialize(split, context);
		
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		if(lineRecordReader.nextKeyValue()){
		
			Text line = lineRecordReader.getCurrentValue();
			if(line.toString().startsWith("Year")){
				key =null;
				value  = null;
			}else{
			String[] array = line.toString().split(",");
			if(array.length > 18 ){
				try{
					
				int day = Integer.parseInt(array[2]);
				int month = Integer.parseInt(array[1]);
				int year = Integer.parseInt(array[0]);
				int delay = Integer.parseInt(array[14]);
				key = new FlightDate(day, month, year, delay);
				value = line; 
				
				
				}catch(Exception e){
					key =null;
					value  = null;
				}
				
			}else{
				key =null;
				value  = null;
			}
			}
			return true;
			
		}else{
			
		}
		
		return false;
	}

	@Override
	public FlightDate getCurrentKey() throws IOException, InterruptedException {
		return key;
	}

	@Override
	public Text getCurrentValue() throws IOException, InterruptedException {
		return value;
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		return lineRecordReader.getProgress();
	}

	@Override
	public void close() throws IOException {
		lineRecordReader.close();
		
	}

}
