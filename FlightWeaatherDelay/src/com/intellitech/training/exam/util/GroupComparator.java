package com.intellitech.training.exam.util;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

import com.intellitech.training.exam.FlightDate;

public class GroupComparator extends WritableComparator {

	public GroupComparator(){
		
		super(FlightDate.class, true);
	}
	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		FlightDate x = (FlightDate) a;
		FlightDate y = (FlightDate) b;
		
		int codeComp = x.getYear().compareTo(y.getYear());
		if(codeComp != 0){
			return codeComp;
		}else{
			codeComp = x.getMonth().compareTo(y.getMonth());
			if(codeComp != 0){
				return codeComp;
			}else{
				codeComp = x.getDay().compareTo(y.getDay());
				return codeComp;
				
			}
		}
	}
	
	

}
