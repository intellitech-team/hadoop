package com.intellitech.training.exam.util;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Partitioner;

import com.intellitech.training.exam.FlightDate;

public class WeatherDelayPartionner extends Partitioner<FlightDate, Text> {

	@Override
	public int getPartition(FlightDate key, Text value, int numPartitions) {
		if(key.getDay().get() < 8){
			return 0;
		}else
		{
			return 1;
		}
		
	}

}
