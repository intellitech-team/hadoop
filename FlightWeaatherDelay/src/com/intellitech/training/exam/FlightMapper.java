package com.intellitech.training.exam;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class FlightMapper extends Mapper<FlightDate, Text, FlightDate, Text> {

	@Override
	protected void map(FlightDate key, Text value, Mapper<FlightDate, Text, FlightDate, Text>.Context context)
			throws IOException, InterruptedException {
		
		if (key != null && value != null) {
			String[] arrayValue = value.toString().split(",");
			if (arrayValue[17].equals("SFO")) {
				String result = "dlay:" + arrayValue[4] + "," + arrayValue[6] + "," + arrayValue[8] + ","
						+ arrayValue[9] + "," + arrayValue[11] + ",|" + arrayValue[14] + "|," + arrayValue[15] + ","
						+ arrayValue[16] + "," + arrayValue[17];
				context.write(key, new Text(result));
			}
		}
	}

}
