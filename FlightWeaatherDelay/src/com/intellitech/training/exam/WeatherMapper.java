package com.intellitech.training.exam;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class WeatherMapper extends Mapper<FlightDate, Text, FlightDate, Text> {

	@Override
	protected void map(FlightDate key, Text value, Mapper<FlightDate, Text, FlightDate, Text>.Context context)
			throws IOException, InterruptedException {
		if(key != null && value != null){
			String[] arrayValue = value.toString().split(",");
			String result =  "wthr:" + arrayValue[4] + "," 
					+ arrayValue[5] + "," 
					+ arrayValue[6] + ",";
			context.write(key, new Text(result));
		}
	}

}
