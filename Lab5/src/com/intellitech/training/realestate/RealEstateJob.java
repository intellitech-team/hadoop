package com.intellitech.training.realestate;

import java.net.URI;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.intellitech.training.realestate.model.AddressCityWritable;
import com.intellitech.training.realestate.model.PostalCodePriceWritable;
import com.intellitech.training.realestate.reader.RealEstateInputFormat;


public class RealEstateJob extends Configured implements Tool{

	@Override
	public int run(String[] args) throws Exception {
		
		Job job = Job.getInstance(getConf(), "lab5");
		job.setJarByClass(getClass());
		
		job.setInputFormatClass(RealEstateInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		TextInputFormat.setInputPaths(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[2]));
		
		job.setMapperClass(RealEstateMapper.class);
		job.setReducerClass(RealEstateReducer.class);
		job.addCacheFile(new URI(args[1]));
		job.setOutputKeyClass(PostalCodePriceWritable.class);
		job.setOutputValueClass(Text.class);
		job.setNumReduceTasks(2);
		job.setMapOutputValueClass(AddressCityWritable.class);
		//Secondary Sort
		job.setPartitionerClass(RealEstatePartitioner.class);
		job.setSortComparatorClass(SortComparator.class);
		//job.setGroupingComparatorClass(TP5GroupComparator.class);
		
		Path output = new Path(args[2]);
		output.getFileSystem(job.getConfiguration()).delete(output, true);
		
		int r = job.waitForCompletion(true) ? 0 : 1 ;
		
		System.out.println(job.getCounters().findCounter("UserCounters","numberOfLines").getValue());
		System.out.println(job.getCounters().findCounter("UserCounters","numberOfnextKeyValueAppel").getValue());
		
		return r;	
	}
	
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new RealEstateJob(),
				new String[] { "hdfs://localhost:9000/training/lab5/inputs/houses.txt",
						"hdfs://localhost:9000/training/lab5/inputs/cities.txt",
						"hdfs://localhost:9000/training/lab5/output/" 
					});
		System.exit(exitCode);
	}

}
