package com.intellitech.training.realestate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import com.intellitech.training.realestate.model.AddressCityWritable;
import com.intellitech.training.realestate.model.PostalCodePriceWritable;

public class RealEstateReducer extends Reducer<PostalCodePriceWritable, AddressCityWritable,PostalCodePriceWritable, Text>{

	private HashMap<IntWritable,Text> cityMap = new HashMap<>();
	
   @Override
   protected void setup(
		Reducer<PostalCodePriceWritable, AddressCityWritable, PostalCodePriceWritable, Text>.Context context)
				throws IOException, InterruptedException {
	   URI uri =context.getCacheFiles()[0];
		Path cacheFile = new Path(uri);
		FSDataInputStream inStream = cacheFile.getFileSystem(context.getConfiguration()).open(cacheFile);
		
		String line;
		try(BufferedReader br = new BufferedReader(new InputStreamReader(inStream))) {
			while ((line =br.readLine()) != null) {
				String[] fields = line.split("\t");
				cityMap.put(new IntWritable(Integer.parseInt(fields[0])),new Text(fields[1]));
			}
		}
}
	
	@Override
	protected void reduce(PostalCodePriceWritable key, Iterable<AddressCityWritable> values,
			Reducer<PostalCodePriceWritable, AddressCityWritable, PostalCodePriceWritable, Text>.Context context)
					throws IOException, InterruptedException {
	
		
		for(AddressCityWritable value : values)  {
			IntWritable keyVille=value.getCityId();
			String val=value.getAddress().toString() + " " + cityMap.get(keyVille);
			context.write(key,new Text(val));
		}
	}
}
