package com.intellitech.training.realestate.model;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;

public class PostalCodePriceWritable implements WritableComparable<PostalCodePriceWritable>{

	private IntWritable codePostal;
	private IntWritable price;
	 
	public PostalCodePriceWritable() {
		this.codePostal = new IntWritable();
		this.price = new IntWritable();
	}
	
	public PostalCodePriceWritable(String codePostal, String valeur) {
		this.codePostal = new IntWritable(Integer.parseInt(codePostal));
		this.price = new IntWritable(Integer.parseInt(valeur));
	}
	
	public IntWritable getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(IntWritable codePostal) {
		this.codePostal = codePostal;
	}

	public IntWritable getPrice() {
		return price;
	}

	public void setPrice(IntWritable price) {
		this.price = price;
	}

	@Override
	public int hashCode() {
		return codePostal.hashCode() + price.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PostalCodePriceWritable) {
			PostalCodePriceWritable doubleIntWritable = (PostalCodePriceWritable) obj;
			return this.getCodePostal().equals(doubleIntWritable.getCodePostal())
					&& this.getPrice().equals(doubleIntWritable.getPrice());
		}
		return false;
	}

	@Override
	public String toString() {
		return " postal code :  "+ codePostal.toString()+ " price :  "+ price.toString();
	}
	@Override
	public void readFields(DataInput dataInput) throws IOException {
		codePostal.readFields(dataInput);
		price.readFields(dataInput);
	}

	@Override
	public void write(DataOutput dataOutput) throws IOException {
		codePostal.write(dataOutput);
		price.write(dataOutput);
	}

	@Override
	public int compareTo(PostalCodePriceWritable doubleIntWritable) {
	int codeCmp=codePostal.compareTo(getCodePostal());
	if(codeCmp!=0){
		return codeCmp;
	}else {
		return price.compareTo(getPrice());
	}
		
	}
}
