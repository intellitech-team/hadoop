package com.intellitech.training.realestate;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

import com.intellitech.training.realestate.model.PostalCodePriceWritable;

public class SortComparator extends WritableComparator{
	
	private static IntWritable.Comparator comparator = new 
			IntWritable.Comparator();
	
	protected SortComparator() {
		super(PostalCodePriceWritable.class,true);
	}
	
	
	@Override
	public int compare(WritableComparable a, WritableComparable b) {
		PostalCodePriceWritable a1=(PostalCodePriceWritable) a;
		PostalCodePriceWritable b1=(PostalCodePriceWritable) b;
		int codeCmp=comparator.compare(a1.getCodePostal(),b1.getCodePostal());
		if(codeCmp!=0){
			return codeCmp;
		}else {
			return comparator.compare(a1.getPrice(),b1.getPrice());
		}
	}
}
