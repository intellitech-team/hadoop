package com.intellitech.healthcare.cheapestProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

public class ProcedureCheapestProviderReducer extends Reducer<Text, Text, Text, Text> {

	MultipleOutputs<Text, Text > mo ;
	private HashMap<Text,Text> proceduresMap = new HashMap<>();
	@Override
	protected void reduce(Text key, Iterable<Text> values,
			Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {
		String firstvalue = values.iterator().next().toString();

		String[] tab = firstvalue.split("--");

			String provider = tab[0];
			String priceStr = tab[1];
			float price, min = Float.parseFloat(priceStr);
			for(Text value  : values){
				tab = value.toString().split("--");
				priceStr = tab[1];
				price = Float.parseFloat(priceStr);
				if(price < min){
					provider = tab[0];
					min = price;
				}
			}
			
			context.write(new Text(key + " " + provider), new Text(min+""));
			mo.write("output1", new Text(key + " " + provider), new Text(min+""));
		
		
	}
	@Override
	protected void setup(Reducer<Text, Text, Text, Text>.Context context) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		mo = new MultipleOutputs<Text,Text>(context);
		URI uri = context.getCacheFiles()[0];
		Path cacheFile = new Path(uri);
		FSDataInputStream inStream = cacheFile.getFileSystem(context.getConfiguration()).open(cacheFile);
		
		String line;
		try(BufferedReader br = new BufferedReader(new InputStreamReader(inStream))) {
			while ((line =br.readLine()) != null) {
				String[] fields = line.split("\t");
				proceduresMap.put(new Text(fields[0]),new Text(fields[1]));
			}
		}
		
	}

}
