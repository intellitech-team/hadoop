package com.intellitech.healthcare.cheapestProvider;

import java.net.URI;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.intellitech.healthcare.averagechargeperstate.TotalAverageChargeForEachState;
import com.intellitech.healthcare.averagechargeperstate.TotalAverageChargeForEachStateMapper;
import com.intellitech.healthcare.averagechargeperstate.TotalAverageChargeForEachStateReducer;

public class ProcedureCheapestProviderMain extends Configured implements Tool{

	@Override
	public int run(String[] args) throws Exception {
		
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "ProcedureCheapestProviderMain");
		job.setJarByClass(TotalAverageChargeForEachState.class);
		
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		TextInputFormat.setInputPaths(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));
		MultipleOutputs.addNamedOutput(job, "output1", TextOutputFormat.class, Text.class, Text.class);
		job.addCacheFile( new URI("hdfs://localhost:9000/medicare/procedures_average_price.txt"));
		job.setPartitionerClass(ProcedureCheapestProviderPartitionner.class);
		job.setMapperClass(ProcedureCheapestProviderMapper.class);
		job.setReducerClass(ProcedureCheapestProviderReducer.class);
		job.setCombinerClass(ProcedureCheapestProviderCombiner.class);
		job.setNumReduceTasks(3);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);
		
		Path outputFile = new Path(args[1]);
		outputFile.getFileSystem(conf).delete(outputFile, true);
		return job.waitForCompletion(true) ? 0 : 1;
	}
	
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new ProcedureCheapestProviderMain(), new String[] { "hdfs://localhost:9000/medicare/Inpatient_big.csv",
				"hdfs://localhost:9000/medicare/output/ProcedureCheapestProviderMain2" });
		
		System.exit(exitCode);

	}
}
