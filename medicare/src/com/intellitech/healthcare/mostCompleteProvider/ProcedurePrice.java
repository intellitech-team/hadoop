package com.intellitech.healthcare.mostCompleteProvider;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.Writable;

public class ProcedurePrice implements Writable {
	
	private Text procedure;
	private FloatWritable price;
	
	public ProcedurePrice(){
		procedure = new Text();
		price = new FloatWritable();
	}
	
	public ProcedurePrice(Text procedure, FloatWritable price) {
		this.procedure = procedure;
		this.price = price;
	}

	
	
	public Text getProcedure() {
		return procedure;
	}
	
	public FloatWritable getPrice() {
		return price;
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		procedure.readFields(in);
		price.readFields(in);
	}
	@Override
	public void write(DataOutput out) throws IOException {
		procedure.write(out);
		price.write(out);
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((procedure == null) ? 0 : procedure.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProcedurePrice other = (ProcedurePrice) obj;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (procedure == null) {
			if (other.procedure != null)
				return false;
		} else if (!procedure.equals(other.procedure))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return procedure + " " + price;
	}

}