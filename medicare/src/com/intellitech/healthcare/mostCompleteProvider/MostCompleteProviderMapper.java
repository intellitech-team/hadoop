package com.intellitech.healthcare.mostCompleteProvider;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MostCompleteProviderMapper extends Mapper<ProviderState, ProcedurePrice, ProviderState, Text> {

	@Override
	protected void map(ProviderState key, ProcedurePrice value,
			Mapper<ProviderState, ProcedurePrice, ProviderState, Text>.Context context)
			throws IOException, InterruptedException {
		//System.out.println("----> Calling  map");
		context.write(key, value.getProcedure());
	}

}
