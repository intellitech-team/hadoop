package com.intellitech.healthcare.procedureCheapPlace;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import com.intellitech.healthcare.averagechargeperstate.TotalAverageChargeForEachState;
import com.intellitech.healthcare.averagechargeperstate.TotalAverageChargeForEachStateMapper;
import com.intellitech.healthcare.averagechargeperstate.TotalAverageChargeForEachStateReducer;

public class ProcedureCheaperPlaceMain extends Configured implements Tool{

	private Long procedureCode = 39L;
	@Override
	public int run(String[] args) throws Exception {
		
		Configuration conf = new Configuration();
		Job job = Job.getInstance(conf, "ProcedureCheaperPlaceMain");
		conf.setLong("procedureCode", procedureCode);
		job.setJarByClass(TotalAverageChargeForEachState.class);
		
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);

		TextInputFormat.setInputPaths(job, new Path(args[0]));
		TextOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setMapperClass(ProcedureCheaperPlaceMapper.class);
		job.setReducerClass(ProcedureCheaperPlaceReducer.class);
		job.setCombinerClass(ProcedureCheaperPlaceReducer.class);

		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(FloatWritable.class);
		Path outputFile = new Path(args[1]);
		outputFile.getFileSystem(conf).delete(outputFile, true);
		return job.waitForCompletion(true) ? 0 : 1;
	}
	
	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new ProcedureCheaperPlaceMain(), new String[] { "hdfs://localhost:9000/medicare/*",
				"hdfs://localhost:9000/medicare/output/ProcedureCheaperPlaceMain" });
		
		System.exit(exitCode);

	}
}
